package com.boogie666.spaceShooter.java;


import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.boogie666.spaceShooter.core.SpaceShooter;

public class SpaceShooterDesktop {
	public static void main (String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("UnJoc");
		config.setWindowedMode(SpaceShooter.WIDTH, SpaceShooter.HEIGHT);
		config.useVsync(true);
		config.setIdleFPS(60);
		new Lwjgl3Application(new SpaceShooter(), config);
	}
}