package com.boogie666.spaceShooter.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import com.boogie666.spaceShooter.core.SpaceShooter;

public class SpaceShooterDesktop {
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = SpaceShooter.WIDTH;
		config.height = SpaceShooter.HEIGHT;
		config.title = "Space Shooter";
		config.vSyncEnabled = true;
		config.foregroundFPS = 60;
		new LwjglApplication(new SpaceShooter(), config);
	}
}
