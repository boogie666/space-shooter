package com.boogie666.spaceShooter.core.behaviours;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import engine.behaviour.Behaviour;
import engine.components.Transform;

public class ScaleTransition extends Behaviour {

	private Transform tf;
	private final float initialScale;
	private final float finalScale;
	
	private boolean done;
	private float timer;
	private final float totalTime;
	
	public ScaleTransition(float initialScale, float finalScale, float time) {
		this.initialScale = initialScale;
		this.finalScale = finalScale;
		this.totalTime = time;
		this.timer = 0;
	}
	
	@Override
	protected void create() {
		this.tf = get(Transform.class);
		this.done = false;
		tf.setScale(initialScale);
	}
	
	@Override
	protected void tick(float deltaTime) {
		if(done) {
			return;
		}
		
		timer += deltaTime;
		final float progress = MathUtils.map(0, totalTime, 0, 1, timer);
		final Vector2 scale = tf.getScale();
		final float value = MathUtils.lerp(initialScale, finalScale, progress);
		scale.x = value;
		scale.y = value;
		if(value >= finalScale) {
			done = true;
			tf.setScale(finalScale);
			return;
		}
		
		tf.setScale(scale);
	}

}
