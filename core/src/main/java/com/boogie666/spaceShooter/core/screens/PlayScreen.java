package com.boogie666.spaceShooter.core.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.boogie666.spaceShooter.core.GameState;
import com.boogie666.spaceShooter.core.Prefabs;
import com.boogie666.spaceShooter.core.ui.HUD;
import com.boogie666.spaceShooter.core.util.CameraShake;

import engine.Entity;

public class PlayScreen extends ScreenAdapter{

	private final SpriteBatch batch;
	private final BitmapFont font;
	private final Entity scene;
	private final GameState gameState;
	private final HUD hud;
	private final Music backgroundMusic;
	private final Prefabs prefabs;
	private final OrthographicCamera camera;
	private final CameraShake shaker;
	
	public PlayScreen(AssetManager assets) {
		this.camera = new OrthographicCamera();
		this.camera.setToOrtho(false);
		this.shaker = new CameraShake(camera);
		this.batch = new SpriteBatch();
		this.font = new BitmapFont();
		this.gameState = new GameState();
		this.scene = new Entity();
		this.prefabs = new Prefabs(scene, gameState, this.shaker, assets);
		this.hud = new HUD(gameState, assets.get("player.png"));
		backgroundMusic = assets.get("background.mp3", Music.class);
		backgroundMusic.setLooping(true);
//		backgroundMusic.play();
		backgroundMusic.setVolume(0);

		init();
	}

	@Override
	public void resize(int width, int height) {
		this.camera.setToOrtho(false, width, height);
	}

	private void init() {
		gameState.reset();
		scene.clear();
		prefabs.scrollingBackground(-4, "background/layer 0.png", 20);
		prefabs.scrollingBackground(-3, "background/layer 1.png", 300);
		prefabs.scrollingBackground(-2, "background/layer 2.png", 900);
		prefabs.scrollingBackground(-1, "background/layer 3.png", 1200);
		prefabs.scrollingBackground(10, "background/layer 4.png", 1500);
		prefabs.scrollingBackground(11, "background/layer 5.png", 1200);
		prefabs.scrollingBackground(12, "background/layer 6.png", 2000);
		prefabs.scrollingBackground(13, "background/layer 7.png", 3000);

		prefabs.player();
		prefabs.spawner(Gdx.graphics.getHeight() + 100);
	}

	@Override
	public void render(float delta) {
		this.shaker.update(delta);
		this.camera.update();
		int fps = Gdx.graphics.getFramesPerSecond();
		if (!gameState.isPaused()) {
			this.scene.update(delta);
		}
		if (gameState.isGameOver() && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
			init();
		} else if (gameState.isPaused() && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
			gameState.setPaused(false);
//			backgroundMusic.play();
		} else if (!gameState.isPaused() && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
			gameState.setPaused(true);
//			backgroundMusic.pause();
		}

		if (gameState.isGameOver()) {
			backgroundMusic.setVolume(MathUtils.lerp(backgroundMusic.getVolume(), 0, delta));
		} else {
			backgroundMusic.setVolume(MathUtils.lerp(backgroundMusic.getVolume(), 0.5f, delta));
		}

		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		this.batch.setProjectionMatrix(camera.combined);
		this.batch.begin();
		this.scene.render(batch);
		this.font.draw(batch, "FPS: " + fps, 20, Gdx.graphics.getHeight() - 20);
		this.font.draw(batch, "Score: " + gameState.getScore(), 20, Gdx.graphics.getHeight() - 40);
		this.font.draw(batch, "Lives: " + gameState.getLives(), 20, Gdx.graphics.getHeight() - 60);
		this.font.draw(batch, "Entity Count: " + scene.getChildren().size(), 20, Gdx.graphics.getHeight() - 80);
		if (gameState.isGameOver()) {
			this.font.draw(batch, "Game Over!", 100, 100);
		}
		hud.render(batch);
		this.batch.end();
	}

	@Override
	public void hide() {
		this.batch.dispose();
	}

}
