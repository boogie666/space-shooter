package com.boogie666.spaceShooter.core.behaviours;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.GameState;
import com.boogie666.spaceShooter.core.Prefabs;

import engine.Entity;
import engine.behaviour.Behaviour;
import engine.components.Collider;
import engine.components.Transform;

public class Player extends Behaviour {

	private Prefabs prefabs;
	private Transform tf;
	private Collider colider;
	private CameraShaker shaker;
	private GameState state;
	private float cooldown = 0.2f;
	private float fireTime = 0;
	private int counter = 0;
	private float maneuverability = 2.5f;

	public Player(Prefabs prefabs) {
		this.prefabs = prefabs;
	}

	@Override
	protected void create() {
		this.tf = get(Transform.class);
		this.colider = get(Collider.class);
		this.shaker = get(CameraShaker.class);
		this.state = get(GameState.class);
		tf.setPosition(Gdx.graphics.getWidth() / 2, 100);
	}

	@Override
	protected void tick(float deltaTime) {
		Vector2 position = tf.getPosition();
		
		float targetX = Gdx.input.getX();
		float targetY = Gdx.graphics.getHeight() - Gdx.input.getY();
	
		
		float x = MathUtils.lerp(position.x, targetX, deltaTime * maneuverability);
		float y = MathUtils.lerp(position.y, targetY, deltaTime * maneuverability);

		tf.setPosition(x, y);

		fireTime -= deltaTime;
		if (Gdx.input.isButtonPressed(Input.Buttons.LEFT) & fireTime <= 0) {
			fireTime = cooldown;
			if (counter % 2 == 0)
				prefabs.bullet(x + 18, y + 20, Vector2.Y);
			else
				prefabs.bullet(x - 18, y + 20, Vector2.Y);
			counter++;
		}

		List<Entity> collisions = this.colider.getCollisions();
		for (Entity e : collisions) {
			if (e.has(Enemy.class)) {
				Vector2 ePos = e.get(Transform.class).getPosition();
				prefabs.explosion(ePos.x, ePos.y);
				e.destroy();
				this.shaker.shake(30, 0.5f);
				state.setLives(state.getLives() - 1);
				if (state.getLives() <= 0) {
					state.setGameOver(true);
					entity.destroy();
				} else {
					entity.add(prefabs.shield());
				}
				break;
			}
		}
	}

	@Override
	protected void destroy() {
		final Vector2 position = tf.getPosition();
		prefabs.explosion(position.x, position.y);
	}

}
