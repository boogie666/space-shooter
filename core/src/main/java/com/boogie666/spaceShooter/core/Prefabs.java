package com.boogie666.spaceShooter.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.behaviours.Bullet;
import com.boogie666.spaceShooter.core.behaviours.CameraShaker;
import com.boogie666.spaceShooter.core.behaviours.Enemy;
import com.boogie666.spaceShooter.core.behaviours.EnemySpawner;
import com.boogie666.spaceShooter.core.behaviours.Explosion;
import com.boogie666.spaceShooter.core.behaviours.Player;
import com.boogie666.spaceShooter.core.behaviours.ScaleTransition;
import com.boogie666.spaceShooter.core.behaviours.ScrollingBackground;
import com.boogie666.spaceShooter.core.behaviours.Shield;
import com.boogie666.spaceShooter.core.behaviours.TimeToLive;
import com.boogie666.spaceShooter.core.util.CameraShake;

import engine.Entity;
import engine.components.AnimationRenderer;
import engine.components.Collider;
import engine.components.CollisionShape;
import engine.components.SoundEmitter;
import engine.components.TextureRenderer;
import engine.components.Transform;

public class Prefabs {

	private final AssetManager assets;
	private final Entity root;
	private final CameraShake shaker;
	private final GameState state;

	public Prefabs(Entity root, GameState state, CameraShake shaker, AssetManager assets) {
		this.root = root;
		this.state = state;
		this.shaker = shaker;
		this.assets = assets;
	}

	public final void bullet(float x, float y, Vector2 direction) {
		final Texture texture = assets.get("bullet.png", Texture.class);
		float scale = 0.7f;
		Transform tf = new Transform();
		tf.setPosition(x, y);
		tf.setScale(scale);
		tf.setRotation(-direction.angle(Vector2.Y));
		Entity bullet = Entity.with(tf, state, new CameraShaker(shaker), new TextureRenderer(texture),
				new CollisionShape(texture.getWidth() * scale, texture.getHeight() * scale), new Collider(),
				new SoundEmitter(assets.get("fire.wav", Sound.class), 0.25f), new TimeToLive(1f),
				new Bullet(500, direction));

		bullet.setPriority(1);

		root.add(bullet);
	}

	public final void player() {
		final Texture texture = assets.get("player.png", Texture.class);
		float scale = 0.7f;
		Transform tf = new Transform();
		tf.setScale(scale);
		Entity player = Entity.with(tf, state, new CameraShaker(shaker), new TextureRenderer(texture),
				new CollisionShape(texture.getWidth() * scale, texture.getHeight() * scale), new Collider(),
				new Player(this));

		player.setPriority(2);
		player.add(this.shield());
		player.add(this.exhaust(+27, -40));
		player.add(this.exhaust(-27, -40));
		root.add(player);
	}

	public final Entity exhaust(float x, float y) {
		final Texture texture = assets.get("exaust.png", Texture.class);
		final Transform tf = new Transform();
		tf.setPosition(x, y);
		tf.setScale(.5f, 0.5f);
		final TextureRegion[][] frames = TextureRegion.split(texture, texture.getWidth() / 4, texture.getHeight());

		return Entity.with(tf, new AnimationRenderer(frames[0], 0.1f, true, true));
	}

	public final Entity shield() {
		final Texture texture = assets.get("shield.png", Texture.class);
		return Entity.with(new Transform(), new CameraShaker(shaker), new TextureRenderer(texture),
				new CollisionShape(texture.getWidth(), texture.getHeight()), new TimeToLive(2.0f), new Collider(),
				new ScaleTransition(0.1f, 1f, 0.15f), new Shield(this));
	}

	public final void enemy(float x, float y) {
		final Texture texture = assets.get("enemy.png", Texture.class);
		final Transform tf = new Transform();
		tf.setPosition(x, y);
		tf.setScale(0.5f);
		root.add(Entity.with(tf, new TextureRenderer(texture),
				new CollisionShape(texture.getWidth() / 2, texture.getHeight() / 2), new Enemy(this)));
	}

	public final void spawner(float y) {
		Transform tf = new Transform();
		tf.setPosition(Gdx.graphics.getWidth() / 2, y);
		root.add(Entity.with(tf, new EnemySpawner(this)));
	}

	public final void explosion(float x, float y) {

		String[] explosionAssets = new String[] { "explosion.png", "explosion2.png", "explosion3.png" };

		String explosionAsset = explosionAssets[MathUtils.random(0, explosionAssets.length - 1)];
		final Texture explosion = assets.get(explosionAsset, Texture.class);
		final float scale = MathUtils.random(1.7f, 2.2f);
		final Transform tf = new Transform();
		tf.setRotation(MathUtils.random(0, 360f));
		tf.setPosition(x, y);
		tf.setScale(scale);
		int frameSize = explosion.getWidth() / 5;
		int frameCount = explosion.getWidth() / frameSize * explosion.getHeight() / frameSize;
		TextureRegion[][] split = TextureRegion.split(explosion, frameSize, frameSize);
		TextureRegion[] explosionFrames = new TextureRegion[frameCount];
		int k = 0;
		for (int i = 0; i < split.length; i++) {
			for (int j = 0; j < split[i].length; j++) {
				explosionFrames[k++] = split[i][j];
			}
		}
		root.add(Entity.with(tf, new SoundEmitter(assets.get("explosion.wav", Sound.class), 1f),
				new AnimationRenderer(explosionFrames, MathUtils.random(0.2f, 0.4f)), new Explosion()));
	}

	public final void scrollingBackground(int priority, String texture, float speed) {
		Transform tf = new Transform();
		tf.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		final Entity background = Entity.with(tf,
				new ScrollingBackground(new TextureRegion(assets.get(texture, Texture.class)), speed));
		background.setPriority(priority);
		root.add(background);
	}

}
