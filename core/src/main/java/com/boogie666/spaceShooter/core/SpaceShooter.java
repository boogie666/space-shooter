package com.boogie666.spaceShooter.core;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.boogie666.spaceShooter.core.screens.LoadingScreen;

public class SpaceShooter extends Game {
	AssetManager assetManager;
	public static final int WIDTH = 600;
	public static final int HEIGHT = 900;
	@Override
	public void create() {
		this.assetManager = new AssetManager();
		this.setScreen(new LoadingScreen(this, this.assetManager));

	}

	@Override
	public void dispose() {
		this.assetManager.dispose();
	}
}
