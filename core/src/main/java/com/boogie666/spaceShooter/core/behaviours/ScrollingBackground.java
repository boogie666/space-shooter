package com.boogie666.spaceShooter.core.behaviours;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import engine.behaviour.Behaviour;
import engine.components.Transform;

public class ScrollingBackground extends Behaviour {

	private Transform tf;
	private final TextureRegion texture;
	private final float speed;
	float originalX;

	public ScrollingBackground(TextureRegion texture, float speed) {
		this.speed = speed;
		this.texture = texture;
	}

	@Override
	protected void create() {
		tf = entity.get(Transform.class);
		originalX = tf.getPosition().x;
	}

	@Override
	public void tick(float deltaTime) {
		Vector2 position = tf.getPosition();
		float offsetX = MathUtils.map(0, Gdx.graphics.getWidth(), -200, 200, Gdx.input.getX());
		float offsetY = MathUtils.map(0, Gdx.graphics.getHeight(), -50, 50, Gdx.input.getY());
		position.x = MathUtils.lerp(originalX, originalX - (offsetX * deltaTime), deltaTime);
		position.y = MathUtils.lerp(position.y, position.y - speed + offsetY, deltaTime);
		
		if (position.y <= -texture.getRegionHeight()) {
			position.y = 0;
		}
		tf.setPosition(position);
	}

	@Override
	public void render(SpriteBatch sb) {
		Vector2 position = tf.getPosition();
		Vector2 scale = tf.getScale();
		float rotation = tf.getRotation();
		final int width = this.texture.getRegionWidth();
		final int height = this.texture.getRegionHeight();
		sb.draw(this.texture, position.x - width / 2f, position.y - height / 2f + height, width / 2f, height / 2f,
				width, height, scale.x, scale.y, rotation);

		sb.draw(this.texture, position.x - width / 2f, position.y - height / 2f, width / 2f, height / 2f, width, height,
				scale.x, scale.y, rotation);
	}

}
