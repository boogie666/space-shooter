package com.boogie666.spaceShooter.core.behaviours;

import engine.behaviour.Behaviour;

public class TimeToLive extends Behaviour {

	private float totalTime;

	public TimeToLive(float time) {
		this.totalTime = time;
	}

	@Override
	protected void tick(float deltaTime) {
		totalTime -= deltaTime;
		if (totalTime <= 0) {
			entity.destroy();
		}
	}

}
