package com.boogie666.spaceShooter.core;

public class Events {
	public static final int BULLET_HIT_ENEMY = 1;
	public static final int ENEMY_HIT_PLAYER = 2;
	public static final int GAME_OVER = 3;
}
