package com.boogie666.spaceShooter.core;

import engine.Component;
import engine.Entity;

public class GameState implements Component {
	private int lives = 3;
	private int score = 0;
	private boolean gameOver = false;
	private boolean paused = false;
	private boolean invulnerable = false;

	public int getLives() {
		return lives;
	}

	public int getScore() {
		return score;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setGameOver(boolean b) {
		this.gameOver = b;
	}

	public boolean isGameOver() {
		return this.gameOver;
	}

	
	public boolean isPaused() {
		return paused;
	}
	
	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public void reset() {
		lives = 3;
		score = 0;
		gameOver = false;
	}

	public boolean isInvulnerable() {
		return invulnerable;
	}
	
	public void setInvulnerable(boolean invulnerable) {
		this.invulnerable = invulnerable;
	}
	
	@Override
	public void attachedTo(Entity e) {
	}

	@Override
	public void removedFrom(Entity e) {
	}

}
