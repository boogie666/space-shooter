package com.boogie666.spaceShooter.core.behaviours;

import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.Prefabs;

import engine.behaviour.Behaviour;
import engine.components.Transform;

public class Enemy extends Behaviour{

	private Transform tf;
	private float speed = 300;
	private Prefabs prefabs;
	
	public Enemy(Prefabs prefabs) {
		this.prefabs = prefabs;
	}
	
	@Override
	protected void create() {
		tf = entity.get(Transform.class);
	}
	
	@Override
	protected void tick(float deltaTime) {
		Vector2 position = tf.getPosition();
		position.y -= speed * deltaTime;
		
		if(position.y < -200) {
			entity.getParent().remove(entity);
		}
		tf.setPosition(position);
	}
	
	public void die() {
		Vector2 etf = this.get(Transform.class).getPosition();
		prefabs.explosion(etf.x, etf.y);
		entity.destroy();
	}


}
