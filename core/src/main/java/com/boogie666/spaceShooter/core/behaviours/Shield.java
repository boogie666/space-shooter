package com.boogie666.spaceShooter.core.behaviours;

import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.Prefabs;

import engine.Entity;
import engine.behaviour.Behaviour;
import engine.components.Collider;
import engine.components.Transform;

public class Shield extends Behaviour {
	private final Prefabs prefabs;
	private Transform tf;
	private Collider colider;
	private CameraShaker shaker;
	
	
	public Shield(Prefabs prefabs) {
		this.prefabs = prefabs;
	}
	
	@Override
	protected void create() {
		this.tf = get(Transform.class);
		this.colider = get(Collider.class);
		this.shaker = get(CameraShaker.class);
	}
	@Override
	protected void tick(float deltaTime) {
		
		tf.setRotation(tf.getRotation() + deltaTime * 50);
		
		List<Entity> collisions = this.colider.getCollisions();
		for (Entity e : collisions) {
			if (e.has(Enemy.class)) {
				Vector2 ePos = e.get(Transform.class).getPosition();
				prefabs.explosion(ePos.x, ePos.y);
				this.shaker.shake(10, 0.1f);
				e.destroy();
				break;
			}
		}
	}

}
