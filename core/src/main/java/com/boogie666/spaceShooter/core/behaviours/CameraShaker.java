package com.boogie666.spaceShooter.core.behaviours;

import com.boogie666.spaceShooter.core.util.CameraShake;

import engine.behaviour.Behaviour;

public class CameraShaker extends Behaviour {

	private final CameraShake shaker;
	
	public CameraShaker(CameraShake shaker) {
		this.shaker = shaker;
	}
	

	public void shake(float intesity, float duration) {
		this.shaker.start(intesity, duration);
	}
	
	@Override
	protected void tick(float deltaTime) {
	}

}
