package com.boogie666.spaceShooter.core.behaviours;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.Prefabs;

import engine.behaviour.Behaviour;
import engine.components.Transform;

public class EnemySpawner extends Behaviour {

	private Prefabs prefabs;
	private Transform tf;
	private float timeElapsed;
	private float spawnRate = .25f;
	private float spawnTimer = 0;
	private float patternSwitchInterval = 3f;
	private float patternSwitchTimer = 0;
	private MovingPattern pattern;

	private List<MovingPattern> patterns = new ArrayList<>();

	public EnemySpawner(Prefabs prefabs) {
		this.prefabs = prefabs;
		this.patterns.add(new SinWave());
		this.patterns.add(new CosWave());
		this.patterns.add(new SawtoothWave());

		this.pattern = pickRandom();
	}

	private MovingPattern pickRandom() {
		int position = MathUtils.random(0, this.patterns.size() - 1);
		return this.patterns.get(position);
	}

	@Override
	protected void create() {
		tf = entity.get(Transform.class);
	}

	@Override
	protected void tick(float deltaTime) {
		Vector2 position = tf.getPosition();
		patternSwitchTimer += deltaTime;
		timeElapsed += deltaTime;
		spawnTimer += deltaTime;
		position.x = this.pattern.calculate(timeElapsed);
		if (spawnTimer >= spawnRate) {
			spawnTimer -= spawnRate;
			this.prefabs.enemy(position.x, position.y);
		}

		if (patternSwitchTimer > patternSwitchInterval) {
			patternSwitchTimer -= patternSwitchInterval;
			this.pattern = pickRandom();
		}

		tf.setPosition(position);

	}

	private static interface MovingPattern {
		float calculate(float timeElapsed);
	}

	private static final class SinWave implements MovingPattern {
		@Override
		public float calculate(float timeElapsed) {
			int width = Gdx.graphics.getWidth();
			return MathUtils.sin(timeElapsed) * width / 4f + width / 2f;
		}
	}

	private static final class CosWave implements MovingPattern {
		@Override
		public float calculate(float timeElapsed) {
			int width = Gdx.graphics.getWidth();
			return MathUtils.cos(timeElapsed) * width / 4f + width / 2;
		}
	}

	private static final class SawtoothWave implements MovingPattern {
		@Override
		public float calculate(float timeElapsed) {
			float result = 0;
			for (int n = 1; n < 3; n++) {
				result += MathUtils.sin(2 * n * timeElapsed) / 2 * n;
			}
			int width = Gdx.graphics.getWidth();
			return result * width / 4f + width / 2f;
		}
	}

}
