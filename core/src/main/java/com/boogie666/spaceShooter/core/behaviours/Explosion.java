package com.boogie666.spaceShooter.core.behaviours;

import engine.behaviour.Behaviour;
import engine.components.AnimationRenderer;

public class Explosion extends Behaviour {

	private AnimationRenderer anim;
	
	
	@Override
	protected void create() {
		this.anim = this.get(AnimationRenderer.class);
	}
	
	@Override
	protected void tick(float deltaTime) {
		if(anim.isDone()) {
			entity.destroy();
		}
	}

}
