package com.boogie666.spaceShooter.core.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.boogie666.spaceShooter.core.GameState;


public class HUD {

	final GameState state;
	final TextureRegion livesSprite;

	public HUD(GameState state, Texture livesSprite) {
		this.state = state;
		this.livesSprite = new TextureRegion(livesSprite);
	}

	public void render(SpriteBatch sb) {
		for (int i = 1; i <= state.getLives(); i++) {
			float scale = 0.2f;
			float postionX = i * (10 + livesSprite.getRegionWidth() * scale);
			sb.draw(livesSprite, postionX, 20, 0, 0, livesSprite.getRegionWidth(), livesSprite.getRegionHeight(), scale,
					scale, 0f);
		}
	}

}
