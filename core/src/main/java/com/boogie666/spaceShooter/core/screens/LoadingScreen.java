package com.boogie666.spaceShooter.core.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class LoadingScreen extends ScreenAdapter {

	private final AssetManager assetManager;
	private final Game game;
	private BitmapFont font;
	private SpriteBatch batch;
	private GlyphLayout layout;
	private ShapeRenderer shapeRenderer;

	public LoadingScreen(Game game, AssetManager assetManager) {
		this.game = game;
		this.assetManager = assetManager;
		
	}

	@Override
	public void show() {
		this.font = new BitmapFont();
		this.batch = new SpriteBatch();
		this.shapeRenderer = new ShapeRenderer();
		this.layout = new GlyphLayout();
		
		this.assetManager.load("explosion.png", Texture.class);
		this.assetManager.load("explosion2.png", Texture.class);
		this.assetManager.load("explosion3.png", Texture.class);
		this.assetManager.load("player.png", Texture.class);
		this.assetManager.load("exaust.png", Texture.class);
		this.assetManager.load("shield.png", Texture.class);
		this.assetManager.load("bullet.png", Texture.class);
		this.assetManager.load("enemy.png", Texture.class);
		
		this.assetManager.load("fire.wav", Sound.class);
		this.assetManager.load("explosion.wav", Sound.class);
		this.assetManager.load("hit.wav", Sound.class);
		this.assetManager.load("background.mp3", Music.class);
		
		
		this.assetManager.load("background/layer 0.png", Texture.class);
		this.assetManager.load("background/layer 1.png", Texture.class);
		this.assetManager.load("background/layer 2.png", Texture.class);
		this.assetManager.load("background/layer 3.png", Texture.class);
		this.assetManager.load("background/layer 4.png", Texture.class);
		this.assetManager.load("background/layer 5.png", Texture.class);
		this.assetManager.load("background/layer 6.png", Texture.class);
		this.assetManager.load("background/layer 7.png", Texture.class);
	}

	@Override
	public void render(float delta) {
		if (this.assetManager.update()) {
			this.game.setScreen(new PlayScreen(this.assetManager));
			return;
		}
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float progress = MathUtils.lerp(0, (Gdx.graphics.getWidth() - 40), this.assetManager.getProgress());
		
		this.shapeRenderer.begin(ShapeType.Line);
		this.shapeRenderer.rect(20, Gdx.graphics.getHeight() / 2 - 10, Gdx.graphics.getWidth() - 40, 30);
		this.shapeRenderer.end();
		
		this.shapeRenderer.setColor(Color.DARK_GRAY);
		this.shapeRenderer.begin(ShapeType.Filled);
		this.shapeRenderer.rect(20, Gdx.graphics.getHeight() / 2 - 10, progress, 30);
		this.shapeRenderer.end();

		String text = "Loading: " + Math.round(this.assetManager.getProgress() * 100) + "%";
		layout.setText(font, text);
		this.batch.begin();
		font.draw(this.batch, text, Gdx.graphics.getWidth() / 2f - layout.width / 2, Gdx.graphics.getHeight() / 2 + layout.height / 2 + 5);
		this.batch.end();		
		
	}
	
	@Override
	public void hide() {
		this.shapeRenderer.dispose();
		this.batch.dispose();
		this.font.dispose();
	}
}
