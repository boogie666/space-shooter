package com.boogie666.spaceShooter.core.behaviours;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.boogie666.spaceShooter.core.GameState;

import engine.Entity;
import engine.behaviour.Behaviour;
import engine.components.Collider;
import engine.components.Transform;

public class Bullet extends Behaviour {

	private Transform tf;
	private Collider colider;
	private Vector2 direction;
	private CameraShaker shaker;
	private GameState state;
	private float speed;
	
	public Bullet(float speed, Vector2 direction) {
		this.direction = new Vector2(direction);
		this.speed = speed;
	}

	@Override
	protected void create() {
		tf = this.get(Transform.class);
		colider = this.get(Collider.class);
		shaker = this.get(CameraShaker.class);
		state = this.get(GameState.class);
	}

	@Override
	protected void tick(float deltaTime) {

		Vector2 position = tf.getPosition();

		position.x += direction.x * speed * deltaTime;
		position.y += direction.y * speed * deltaTime;

		tf.setPosition(position);

		if (position.y > Gdx.graphics.getHeight() + 200) {
			this.entity.destroy();
		}

		List<Entity> collisions = colider.getCollisions();
		for (Entity e : collisions) {
			if (e.has(Enemy.class)) {
				e.get(Enemy.class).die();
				entity.destroy();
				this.shaker.shake(10f, 0.1f);
				this.state.setScore(this.state.getScore() + 1);
				break;
			}
		}

	}

}
