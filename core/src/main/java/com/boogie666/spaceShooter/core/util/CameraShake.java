package com.boogie666.spaceShooter.core.util;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.MathUtils;

public class CameraShake {
	private final Camera camera;
	private final float baseX;
	private final float baseY;
	private final float baseZ;
	private float timer = 0;
	private float duration;
	private float intensity;
	
	public CameraShake(Camera camera) {
		this.camera = camera;
		
		this.baseX = camera.position.x;		
		this.baseY = camera.position.y;		
		this.baseZ = camera.position.z;
	}
	
	public void update(float delta) {
		camera.position.set(baseX, baseY, baseZ);
		if (timer > 0) {
			float currentPower = intensity * ((duration - timer) / duration);
			float newX = MathUtils.random(-0.5f, 0.5f) * currentPower;
			float newY = MathUtils.random(-0.5f, 0.5f) * currentPower;
			float newZ = MathUtils.random(0.25f, 0.75f) * currentPower;
			camera.position.x -= newX;
			camera.position.y -= newY;
			camera.position.z += newZ;
			timer -= delta;
		}
	}
	
	
	public void start(float intesity, float duration) {
		this.timer = duration;
		this.duration = duration;
		this.intensity = intesity;
	}
}
