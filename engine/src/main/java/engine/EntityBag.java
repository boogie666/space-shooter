package engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class EntityBag implements Iterable<Entity> {
	private final Entity entity;
	private final List<Entity> entities;
	private final Set<Entity> addedBatch;
	private final Set<Entity> removeBatch;

	private static final class EntityComparator implements Comparator<Entity> {
		static final EntityComparator INSTANCE = new EntityComparator();

		@Override
		public int compare(Entity o1, Entity o2) {
			return Integer.compare(o1.getPriority(), o2.getPriority());
		}

	}

	EntityBag(Entity entity) {
		this.entity = entity;
		this.entities = new ArrayList<Entity>();
		this.addedBatch = new HashSet<Entity>();
		this.removeBatch = new HashSet<Entity>();
	}

	void add(Entity e) {
		this.addedBatch.add(e);
	}

	void remove(Entity e) {
		this.removeBatch.add(e);
	}

	void flush() {
		this.entities.addAll(addedBatch);
		for (Entity e : this.addedBatch) {
			e.setParent(entity);
		}

		this.entities.removeAll(removeBatch);
		for (Entity e : removeBatch) {
			e.setParent(null);
			e.destroy();
		}
		if (addedBatch.size() > 0)
			Collections.sort(this.entities, EntityComparator.INSTANCE);

		addedBatch.clear();
		removeBatch.clear();
	}

	Collection<Entity> getEntities() {
		return entities;
	}

	@Override
	public Iterator<Entity> iterator() {
		return this.entities.iterator();
	}

	void clear() {
		this.removeBatch.addAll(entities);
	}

}
