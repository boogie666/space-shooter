package engine.utils;

import java.util.Iterator;
import java.util.Stack;

import engine.Entity;

public class EntityUtils {

	public static Entity findRoot(Entity e) {
		while (e.getParent() != null) {
			e = e.getParent();
		}
		return e;
	}

	public static Iterable<Entity> traverse(Entity e) {

		return new Iterable<Entity>() {

			@Override
			public Iterator<Entity> iterator() {
				Stack<Entity> stack = new Stack<>();
				stack.push(e);
				return new Iterator<Entity>() {

					@Override
					public boolean hasNext() {
						return !stack.isEmpty();
					}

					@Override
					public Entity next() {
						final Entity current = stack.pop();
						stack.addAll(current.getChildren());
						return current;
					}
				};
			}
		};
	}
}
