package engine.components;

import com.badlogic.gdx.math.Vector2;

import engine.Component;
import engine.Entity;

public class Transform implements Component {
	private Transform parent;
	private final Vector2 position;
	private final Vector2 scale;
	private float rotation;

	public Transform() {
		this.position = new Vector2();
		this.scale = new Vector2(1, 1);
		this.rotation = 0;
	}

	private final Vector2 tmpPos = new Vector2();

	public Vector2 getPosition() {
		if (parent == null) {
			return tmpPos.set(position);
		}
		final Vector2 parentPos = parent.getPosition();
		final float parentRot = parent.getRotation();
		return tmpPos.set(position).add(parentPos).rotateAround(parentPos, parentRot);
	}

	public float getRotation() {
		if(parent == null) {
			return rotation;
		}
		return rotation + parent.getRotation();
	}

	private final Vector2 tmpScale = new Vector2();

	public Vector2 getScale() {
		if (parent == null) {
			return tmpScale.set(scale);
		}
		return tmpScale.set(scale).scl(parent.getScale());
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	public void setPosition(Vector2 position) {
		this.position.set(position);
	}

	public void setPosition(float x, float y) {
		this.position.set(x, y);
	}

	public void setScale(Vector2 scale) {
		this.scale.set(scale);
	}

	public void setScale(float sx, float sy) {
		this.scale.set(sx, sy);
	}

	public void setScale(float s) {
		this.setScale(s, s);
	}

	public void setParent(Transform parentTransform) {
		this.parent = parentTransform;
	}

	public Transform getParent() {
		return parent;
	}

	@Override
	public void attachedTo(Entity go) {
		Entity parent = go.getParent();
		if (parent != null) {
			this.parent = parent.get(Transform.class);
		} else {
			parent = null;
		}
	}

	@Override
	public void removedFrom(Entity go) {
		this.parent = null;
	}

}
