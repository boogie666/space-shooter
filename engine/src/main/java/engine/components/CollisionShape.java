package engine.components;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import engine.Component;
import engine.Entity;

public class CollisionShape implements Component {
	public final Rectangle bounds;
	private Transform transform;

	public CollisionShape(float width, float height) {
		this.bounds = new Rectangle(0, 0, width, height);
	}

	@Override
	public void update(float deltaTime) {
		Vector2 position = transform.getPosition();
		this.bounds.setCenter(position.x, position.y);
	}

	public boolean collidesWith(CollisionShape other) {
		return this.bounds.overlaps(other.bounds);
	}

	@Override
	public void attachedTo(Entity e) {
		this.transform = e.get(Transform.class);
		Vector2 pos = transform.getPosition();
		this.bounds.setCenter(pos.x,pos.y);
	}

	@Override
	public void removedFrom(Entity e) {
	}

	@Override
	public String toString() {
		return this.bounds.toString();
	}

}
