package engine.components;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import engine.Component;
import engine.Entity;

public class AnimationRenderer implements Component{

	private final float timePerFrame;
	private float timer;
	private final TextureRegion[] frames;
	private int currentFrame = 0;
	private Transform transform;
	private boolean looping;
	private boolean started;

	public AnimationRenderer(TextureRegion[] frames, float time, boolean looping, boolean startImediatly) {
		this.timePerFrame = time / frames.length;
		this.frames = frames;
		this.looping = looping;
		this.started = startImediatly;
	}

	public AnimationRenderer(TextureRegion[] frames, float time) {
		this(frames, time, false, true);
	}

	public void setLooping(boolean looping) {
		this.looping = looping;
	}

	public void start() {
		this.started = true;
	}

	public void stop() {
		this.started = false;
	}

	public boolean isDone() {
		return this.currentFrame >= this.frames.length;
	}

	public void reset() {
		this.currentFrame = 0;
		this.timer = 0;
	}

	@Override
	public void render(SpriteBatch sb) {
		if (this.transform != null && started && !isDone()) {
			final TextureRegion texture = this.frames[currentFrame];
			final Vector2 position = transform.getPosition();
			final Vector2 scale = transform.getScale();
			final float rotation = transform.getRotation();
			int width = texture.getRegionWidth();
			int height = texture.getRegionHeight();
			sb.draw(texture, 
					position.x - width / 2f, position.y - height / 2f, 
					width / 2f, height / 2f, 
					width, height,
					scale.x, scale.y, 
					rotation);
		}
	}

	@Override
	public void update(float deltaTime) {
		if (!started) {
			return;
		}
		if (currentFrame >= this.frames.length) {
			return;
		}
		timer += deltaTime;
		if (timer > timePerFrame) {
			currentFrame++;
			timer -= timePerFrame;
		}
		if (currentFrame >= this.frames.length && looping) {
			currentFrame = 0;
		}
	}

	@Override
	public void attachedTo(Entity e) {
		this.transform = e.get(Transform.class);
	}

	@Override
	public void removedFrom(Entity e) {

	}

}
