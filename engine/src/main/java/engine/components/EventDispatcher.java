package engine.components;

import engine.Component;
import engine.Entity;

public class EventDispatcher implements Component {

	private final EventHandler handler;
	private Entity entity;

	public EventDispatcher(EventHandler handler) {
		this.handler = handler;
	}

	public void dispatch(Event e) {
		e.setEntity(this.entity);
		this.handler.handle(e);
	}

	@Override
	public void attachedTo(Entity e) {
		this.entity = e;
	}

	@Override
	public void removedFrom(Entity e) {
	}

}
