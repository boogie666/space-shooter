package engine.components;

public interface EventHandler {

	void handle(Event e);

}
