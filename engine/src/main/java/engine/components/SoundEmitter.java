package engine.components;

import com.badlogic.gdx.audio.Sound;

import engine.Component;
import engine.Entity;

public class SoundEmitter implements Component {

	private final Sound sound;
	private final boolean startOnAttach;
	private float volume;

	public SoundEmitter(Sound sound, float volume, boolean startOnAttach) {
		this.sound = sound;
		this.volume = volume;
		this.startOnAttach = startOnAttach;
	}

	public SoundEmitter(Sound sound, boolean startOnAttach) {
		this(sound, 1, startOnAttach);
	}

	public SoundEmitter(Sound sound) {
		this(sound, true);
	}

	public SoundEmitter(Sound sound, float volume) {
		this(sound, volume, true);
	}

	public void play() {
		this.play(volume);
	}

	public void play(float volume) {
		this.volume = volume;
		this.sound.play(volume);
	}

	@Override
	public void attachedTo(Entity e) {
		if (startOnAttach)
			this.play();
	}

	@Override
	public void removedFrom(Entity e) {
	}

}
