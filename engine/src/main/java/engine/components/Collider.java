package engine.components;

import java.util.ArrayList;
import java.util.List;

import engine.Component;
import engine.Entity;
import engine.utils.EntityUtils;

public class Collider implements Component {
	private Entity me;
	private Entity rootScene;
	private CollisionShape shape;

	@Override
	public void attachedTo(Entity e) {
		this.shape = e.get(CollisionShape.class);
		this.me = e;
		this.rootScene = EntityUtils.findRoot(e);
	}

	public List<Entity> getCollisions() {
		
		List<Entity> result = new ArrayList<>();
		for (Entity e : EntityUtils.traverse(rootScene)) {
			if (e != me && e.has(CollisionShape.class)) {
				CollisionShape otherShape = e.get(CollisionShape.class);
				if (shape.collidesWith(otherShape)) {
					result.add(e);
				}
			}
		}
		return result;
	}

	@Override
	public void removedFrom(Entity e) {
	}
}
