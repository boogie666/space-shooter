package engine;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Component {
	void attachedTo(Entity e);

	void removedFrom(Entity e);

	default void update(float dt) {
	}

	default void render(SpriteBatch sb) {
	}

}
