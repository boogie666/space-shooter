package engine;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

class ComponentBag implements Iterable<Component> {
	private final Map<Class<? extends Component>, Component> components;
	private final Set<Component> addedBatch;
	private final Set<Class<? extends Component>> removedBatch;
	private final Entity entity;

	ComponentBag(Entity entity) {
		this.entity = entity;
		this.components = new LinkedHashMap<>();
		this.addedBatch = new LinkedHashSet<>();
		this.removedBatch = new LinkedHashSet<>();
	}

	void add(Component c) {
		this.addedBatch.add(c);
	}

	void remove(Class<? extends Component> type) {
		this.removedBatch.add(type);
	}

	<T extends Component> T get(Class<T> type) {
		return type.cast(components.get(type));
	}

	boolean has(Class<? extends Component> type) {
		return components.containsKey(type);
	}

	void flush() {
		for (Component c : addedBatch) {
			components.put(c.getClass(), c);
			c.attachedTo(this.entity);
		}
		for (Class<? extends Component> type : removedBatch) {
			Component c = this.components.remove(type);
			c.removedFrom(this.entity);
		}
		this.addedBatch.clear();
		this.removedBatch.clear();
	}

	@Override
	public Iterator<Component> iterator() {
		return this.components.values().iterator();
	}

	void clear() {
		for(Component c : this) {
			this.removedBatch.add(c.getClass());
		}
	}

}
