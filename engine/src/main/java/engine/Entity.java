package engine;

import java.util.Collection;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Entity {

	private final ComponentBag components;
	private final EntityBag children;
	private Entity parent;
	private int priority;
	private boolean enabled;

	public Entity() {
		this.components = new ComponentBag(this);
		this.children = new EntityBag(this);
		this.enabled = true;
	}

	public Entity attach(Component c) {
		components.add(c);
		return this;
	}

	public Entity add(Entity child) {
		this.children.add(child);
		return this;

	}

	public void remove(Entity child) {
		this.children.remove(child);
	}

	public <T extends Component> T get(Class<T> type) {
		return components.get(type);
	}

	public void detach(Class<? extends Component> type) {
		components.remove(type);
	}

	public boolean has(Class<? extends Component> type) {
		return components.has(type);
	}

	public void update(float dt) {
		if (enabled) {
			for (Component c : components) {
				c.update(dt);
			}
			for (Entity child : children) {
				child.update(dt);
			}
		}
		components.flush();
		children.flush();
	}

	public void render(SpriteBatch sb) {
		if (enabled) {
			for (Component c : components) {
				c.render(sb);
			}
			for (Entity child : children) {
				child.render(sb);
			}
		}
	}

	public void setParent(Entity parent) {
		if (this.parent != parent && parent != null) {
			this.parent = parent;

			for (Component c : this.components) {
				c.attachedTo(this);
			}
		}

	}

	public Entity getParent() {
		return parent;
	}

	public Collection<Entity> getChildren() {
		return children.getEntities();
	}

	public void destroy() {
		this.parent.remove(this);
		for(Component c : this.components) {
			this.detach(c.getClass());
		}
	}

	public void clear() {
		this.components.clear();
		this.children.clear();
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public static Entity with(Component... components) {
		Entity e = new Entity();
		for (Component c : components) {
			e.attach(c);
		}
		return e;
	}

}
